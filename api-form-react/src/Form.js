import React from 'react'
import Api from './Api';

class Form extends React.Component {
    constructor(state) {
        super(state);
        let placeholder = "";
        this.state = {
            url: "",
            apiComponent: null,
            choice: "",
        }
    }


    handleChange = (event) => {
        let value = event.target.value.toLowerCase();
        this.setState({ url: "https://pokeapi.co/api/v2/pokemon/" + value });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.state.url === "") {
            alert("Please enter search criteria.")
        }
        else {
            this.setState({ apiComponent: <Api url={this.state.url} choice={this.state.choice} /> });
        }
    }

    handleReset = (event) => {
        event.preventDefault();
        Array.from(document.querySelectorAll("input")).forEach(
            input => (input.value = "")
        );
        this.setState({
            url: "",
            apiComponent: null,
        });
    }

    setChoice = (value) => {
        if (value === "name") {
            this.placeholder = "Pokemon Number";
            this.setState({
                choice: value,
            });
        }
        else {
            this.placeholder = "Pokemon Name or Number";
            this.setState({
                choice: value,
            });
        }
    }

    render() {
        return (
            <div className="Form">
                <form id="pokemonForm" onSubmit={this.handleSubmit}>
                    <span>Enter Search Criteria</span>
                    <br /><br />
                    <input type='radio' name="choice" defaultValue="abilities" onClick={(e) => this.setChoice(e.target.value)} />Pokemon Abilities
                    <input type='radio' name="choice" defaultValue="moves" onClick={(e) => this.setChoice(e.target.value)} />Moves
                    <input type='radio' name="choice" defaultValue="types" onClick={(e) => this.setChoice(e.target.value)} />Types
                    <input type='radio' name="choice" defaultValue="name" onClick={(e) => this.setChoice(e.target.value)} />Pokemon Name
                    <br /><br />
                    <input type='text' placeholder={this.placeholder} value={this.state.value} onChange={this.handleChange} />
                    <br /><br />
                    <input type="submit"></input>
                    <br />
                    <button onClick={this.handleReset}>Reset</button>
                </form>
                <br />
                {this.state.apiComponent}
            </div>
        );
    }
}

export default Form;
