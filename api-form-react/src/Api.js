import React from 'react';
import Display from './Display'

class Api extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
        };
    }

    addAbilities = (data) => {
        let counter = 1;
        let abilities = [];

        data.abilities.map((ability, index) => {
            abilities.push("Ability " + counter + ": " + data.abilities[index].ability.name);
            counter++;
        })

        return abilities;
    }

    addMoves = (data) => {
        let counter = 1;
        let moves = [];

        data.abilities.map((move, index) => {
            moves.push("Move " + counter + ": " + data.moves[index].move.name);
            counter++;
        });

        return moves;
    }

    addTypes = (data) => {
        let counter = 1;
        let types = [];

        data.types.map((type, index) => {
            types.push("Type " + counter + ": " + data.types[index].type.name);
            counter++;
        });

        return types;
    }

    addName = (data) => {
        let name = [];
        name.push("Name: " + data.forms[0].name);
        return name;
    }

    displayData = () =>
        this.state.items.map(
            (items, index) =>
                <Display key={index} items={this.state.items[index]} />
        );

    componentDidMount() {
        fetch(this.props.url)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                let responseData = "";
                if (this.props.choice === "abilities") {
                    responseData = this.addAbilities(data);
                } else if (this.props.choice === "moves") {
                    responseData = this.addMoves(data);
                } else if (this.props.choice === "types") {
                    responseData = this.addTypes(data);
                } else if (this.props.choice === "name") {
                    responseData = this.addName(data);
                }
                this.setState({
                    isLoaded: true,
                    items: responseData,
                })
            },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        console.log(this.state.items);
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: Could not find the Pokemon entered.<br />
                Please select reset and try again.</div>;
        }
        else if (!isLoaded) {
            return <div id="container">
                Loading...
            </div>
        }
        else {
            return (
                <div>
                    {this.displayData()}
                </div>
            )
        }
    }
}

export default Api;